# Mon Livret de Maternelle

**Mon livret de maternelle** est une application en ligne permettant de rédiger un carnet de réussite pour les élèves des classes de TPS à GS à partir d'une liste de compétences définies au sein de l'école.

## Utilisation

1. En partant de zéro (bouton vert sur la page d'accueil), vous êtes redirigés vers les partamètres pour rentrer les informations de votre école, votre classe et un mot d'introduction qui sera ajouté au début de chaque livret.
2. Dans la partie "compétences", vous pouvez ajouter les compétences sélectionnées par l'équipe enseignante dans chacun des domaines. Ces derniers sont entièrement modifiables (ajout, suppression, mot d'introduction, couleur).
3. Dans la partie "élèves", vous pouvez ajouter vos élèves, puis pour chacun d'entre eux, accéder à leur livret pour ajouter des compétences réussies et des commentaires.
4. Lors de l'impression, seules les compétences réussies par l'élève, et ajoutées à son livret, sont imprimées.

Une documentation complète est disponible [via ce lien](https://markpage.forge.apps.education.fr/#https://codimd.apps.education.fr/s/V37kgc7fk#).

## Licences

Cette application est proposée sous la licence [AGPL v3](https://forge.apps.education.fr/mdebesse/livret-maternelle/-/blob/main/LICENSE)

Les éléments suivants sont utilisés :

* Icônes par [Solar Icons](https://www.figma.com/community/file/1166831539721848736?ref=svgrepo.com) sous licence CC-BY
* Les infobulles sont gérées avec [Hint.css](https://github.com/chinchang/hint.css) sous licence MIT
* L'enregistrement sur le navigateur est réalisé avec [localForage](https://github.com/localForage/localForage) sous licence Apache 2