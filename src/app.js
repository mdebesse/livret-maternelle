let elementsIds = ['ecranAccueil','historiqueVersions','ecranEleves','listeEleves','ecranCompetences','listeCompetences','ecranParametres','listeParametres','ecranDetailsEleve','ecranModifEleve','ecranModifEleveTitre','ecranModifEleveNom','ecranModifEleveNaissance','ecranModifEleveBoutons','ecranModifDomaine','ecranModifCompetence','ecranModifDomaineTitre','ecranModifDomaineNom','ecranModifDomaineCouleur','ecranModifDomaineIntro','ecranModifDomaineBoutons','ecranModifCompetenceTitre','ecranModifCompetenceNom','ecranModifCompetenceImage','ecranModifCompetenceBoutons','ecranParametresNom','ecranParametresAnnee','ecranParametresClasse','ecranParametresIntro','formImportJSON','cache','ecranDetailEleveComp','ecranDetailEleveCompTitre','ecranDetailEleveCompDétails','ecranDetailEleveCompDate','ecranDetailEleveCompCommentaire','ecranDetailEleveCompBoutons','ecranCommentaireEleve','ecranCommentaireEleveTitre','ecranCommentaireEleveDate','ecranCommentaireEleveContenu','ecranCommentaireEleveBoutons','formImportJSONboutons','output','ecranAjoutImage','ecranAjoutImageTitre','ecranAjoutImageSource','ecranAjoutImageCommentaire','ecranAjoutImageBoutons','messages','outputImageIllustration','couleurBaniere','Menu','ecranParametresImageIllustration','outputPhoto','ecranCommentaireDomaine','ecranCommentaireDomaineTitre','ecranCommentaireDomaineDate','ecranCommentaireDomaineContenu','ecranCommentaireDomaineBoutons','boiteMulti'];
let elements = {};
elementsIds.forEach(id => {
	elements[id] = document.getElementById(id);
});
let donneesBase = {"parametres":{"ecole":"Nom de l'école","anneeScolaire":"2023-2024","classe":"GS de Mme Dupont","motIntroduction":""},"competences":[{"domaine":"Apprendre ensemble et vivre ensemble","motIntro":"<br>","competences":[],"couleur":"#016e8f"},{"domaine":"Mobiliser le langage dans toutes ses dimensions : L’oral","motIntro":"<br>","competences":[],"couleur":"#0042a9"},{"domaine":"Mobiliser le langage dans toutes ses dimensions : L’écrit","motIntro":"<br>","competences":[],"couleur":"#2c0977"},{"domaine":"Agir, s’exprimer, comprendre à travers l’activité physique","motIntro":"","competences":[],"couleur":"#61187c"},{"domaine":"Agir, s’exprimer, comprendre à travers les activités artistiques","motIntro":"","competences":[],"couleur":"#791a3d"},{"domaine":"Acquérir les premiers outils mathématiques : Découvrir les nombres et leurs utilisations","motIntro":"","competences":[],"couleur":"#b51a00"},{"domaine":"Acquérir les premiers outils mathématiques : Explorer des formes, des grandeurs, des suites organisées","motIntro":"","competences":[],"couleur":"#ad3e00"},{"domaine":"Explorer le monde : Se repérer dans le temps","motIntro":"","competences":[],"couleur":"#a96800"},{"domaine":"Explorer le monde : Se repérer dans l’espace","motIntro":"","competences":[],"couleur":"#c4bc00"},{"domaine":"Explorer le monde : Découvrir le monde vivant","motIntro":"","competences":[],"couleur":"#9ba50e"},{"domaine":"Explorer le monde : Explorer la matière","motIntro":"","competences":[],"couleur":"#4e7a27"},{"domaine":"Explorer le monde : Utiliser, fabriquer, manipuler des objets","motIntro":"","competences":[],"couleur":"#474747"},{"domaine":"Explorer le monde : Utiliser des outils numériques","motIntro":"","competences":[],"couleur":"#707070"}],"eleves":[{"Nom":"Nom Prénom","Naissance":"01/01/2020","Niveau":"GS","Competences":[],"Commentaires":[]}]};
let donnees = JSON.parse(JSON.stringify(donneesBase));
let creationEleve = '';
let imageBase64 = '';
let photoActuelle = {"domaine":"","image":"","commentaire":""};
let interval = 300000;
let intervalId = setInterval(sauvegardeLocale, interval);
let donneesImport = '';
let idImport = [];

/* Vérification de l'enregistrement */
let archive = JSON.stringify(donneesBase);

function majArchive() {
	archive = JSON.stringify(donnees);
}

window.addEventListener('beforeunload', function (e) {
	if (archive !== JSON.stringify(donnees)) {
		cacherTout();
		messages.innerHTML = '<div class="bouton rouge">Des modifications n\'ont pas été enregistrées, êtes vous sûr(e) de vouloir partir ?</div>';
		messages.style.display = 'block';
		e.preventDefault();
		e.returnValue = 'Êtes-vous sûr de fermer l\'onglet ?';
	}
});

/* Mise en place du programme */

initialisation();

function ouvertureFichier(event) {
    let file = event.target.files[0];
    if (file) {
        let reader = new FileReader();
        reader.onload = function(e) {
            let contenu = e.target.result;
            // Traitement du contenu du fichier
            nvFichier = JSON.parse(contenu);
			if (nvFichier && nvFichier["parametres"] && nvFichier["competences"] && nvFichier["eleves"]) {
				donnees = nvFichier;
				majArchive();
				afficherEcran(ecranParametres);
			} else {
				formImportJSONboutons.innerHTML = '<p>Désolé, le fichier n\'a pas été reconnu comme une sauvegarde de <em>Mon Livret Maternelle</em>, veuillez vérifier votre fichier avant de réessayer.</p>';
			}
        };
        reader.readAsText(file);
    }
}

function annulerImportJSON() {
	donnees = JSON.parse(JSON.stringify(donneesBase));
	formImportJSON.value = '';
	formImportJSONboutons.innerHTML = '';
}

function utiliserSauvegardeLocale() {
	localforage.getItem('donnees').then(function(value) {
		if (value !== null) {
			donnees = JSON.parse(value);
			majArchive();
			afficherEcran(ecranParametres);
			messages.innerHTML = '';
		} else {
			messages.innerHTML = '<div>Erreur sur l\'importation des données.</div>';
		}
	}).catch(function(err) {
		messages.innerHTML = '<div>Erreur sur l\'importation des données.</div>';
	});
}

function partirdeZero() {
	annulerImportJSON();
	afficherEcran(ecranParametres);
}

let date = new Date();
let day = String(date.getDate()).padStart(2, '0');
let month = String(date.getMonth() + 1).padStart(2, '0');
let year = date.getFullYear();
let formattedDate = `${day}/${month}/${year}`;

/* Fonctions générales */

function initialisation() { // Vérifier l'existance de données en local
	localforage.getItem('date').then(function(value) {
		if (value !== null) {
			let contenuHTML = '<div class="boite bGris">Des données précédentes datant du '+ value +' ont été trouvées.<br>Souhaitez-vous les conserver ?<br><br><div class="bFormation bRouge main" onclick="effacerSauvegardeLocale()">Effacer ces données</div><div class="bFormation bVert main" onclick="utiliserSauvegardeLocale()">Conserver ces données</div></div>';
			messages.innerHTML = contenuHTML;
		} else {
			messages.innerHTML = '';
		}
	}).catch(function(err) {
		messages.innerHTML = '';
	});
}

function cacherTout() {
	let elementsToHide = [ecranAccueil,historiqueVersions,ecranEleves,ecranCompetences,ecranParametres,ecranDetailsEleve,ecranModifEleve,ecranModifDomaine,ecranModifCompetence,cache,ecranDetailEleveComp,ecranCommentaireEleve,messages,ecranAjoutImage,ecranCommentaireDomaine,boiteMulti];
	elementsToHide.forEach(element => {
		element.style.display = 'none';
	});
	boiteMulti.innerHTML = '';
	donneesImport = '';
	idImport.splice(0, idImport.length);
}

function afficherEcran(id) {
	cacherTout();
	id.style.display = 'block';
	switch (id) {
		case ecranParametres:
			genererParametres();
			break;
		case ecranEleves:
			genererEcranEleves();
			break;
		case ecranCompetences:
			genererEcranCompetences();
			break;
	};
}

function afficherCache() {
	cache.style.display = 'block';
	let hauteurCache = 0;
	if (document.body.scrollHeight > window.innerHeight) {
		hauteurCache = document.body.scrollHeight;
	} else {
		hauteurCache = window.innerHeight;
	}
	cache.style.height = hauteurCache + "px";
}

/* Élèves */

function genererEcranEleves() {
	let contenuHTML = '<div class="grilleQuatre">';
	donnees["eleves"].forEach((element, i) => {
		contenuHTML += '<div class="boite main centrer" onclick="genererDetailsEleve('+i+')"><strong>'+element["Nom"]+'</strong><br><em>'+element["Naissance"]+'</em><br><div class="etiquette b'+element["Niveau"]+' inblo">'+element["Niveau"]+'</div></div>';
	});
	contenuHTML += '</div><br><br><div class="centrer">Fonctions avancées : <br><div class="bFormation main bGris" onclick="afficherImport()">Importer des élèves</div><div class="bFormation main bGris" onclick="afficherExport()">Exporter des élèves</div><div class="bFormation bGris main" onclick="suppressionEnLot()">Supprimer plusieurs élèves</div><br><div class="bFormation main bGris" onclick="afficherChangAnnee()">Changement d\'année scolaire</div></div>';
	listeEleves.innerHTML = contenuHTML;
}

function modificationEleve(id) {
	afficherCache();
	ecranModifEleveTPS.classList.replace('bTPS', 'bGris');
	ecranModifElevePS.classList.replace('bPS', 'bGris');
	ecranModifEleveMS.classList.replace('bMS', 'bGris');
	ecranModifEleveGS.classList.replace('bGS', 'bGris');
	if (id === 'ajout') {
		creationEleve = {"Nom":"","Naissance":"01/01/2024","Niveau":"PS","Competences":[],"Commentaires":[]};
		ecranModifEleveTitre.innerHTML = 'Ajouter un élève';
		ecranModifEleveBoutons.innerHTML = '<div class="bFormation main bViolet" onclick="afficherEcran(ecranEleves)">Annuler</div><div class="bFormation main bVert" onclick="ajouterEleve()">Valider</div>';
		ecranModifEleveNom.value = creationEleve["Nom"];
		ecranModifEleveNaissance.value = creationEleve["Naissance"];
	} else if (id !== '') {
		creationEleve = JSON.parse(JSON.stringify(donnees["eleves"][id]));
		ecranModifEleveTitre.innerHTML = 'Modifier un élève';
		ecranModifEleveNom.value = creationEleve["Nom"];
		ecranModifEleveNaissance.value = creationEleve["Naissance"];
		ecranModifEleveBoutons.innerHTML = '<div class="bFormation main bViolet" onclick="genererDetailsEleve('+id+')">Annuler</div><div class="bFormation main bVert" onclick="modifierEleve('+id+')">Valider</div>';
	} 
	if (creationEleve["Niveau"] === 'TPS') {
		ecranModifEleveTPS.classList.replace('bGris', 'bTPS');
	}
	if (creationEleve["Niveau"] === 'PS') {
		ecranModifElevePS.classList.replace('bGris', 'bPS');
	}
	if (creationEleve["Niveau"] === 'MS') {
		ecranModifEleveMS.classList.replace('bGris', 'bMS');
	}
	if (creationEleve["Niveau"] === 'GS') {
		ecranModifEleveGS.classList.replace('bGris', 'bGS');
	}
	ecranModifEleve.style.display = 'block';
}

function ajouterEleve() {
	creationEleve["Nom"] = ecranModifEleveNom.value;
	creationEleve["Naissance"] = ecranModifEleveNaissance.value;
	donnees["eleves"].push(creationEleve);
	creationEleve = {"Nom":"","Naissance":"01/01/2024","Niveau":"","Competences":[],"Commentaires":[]};
	donnees["eleves"].sort((a, b) => {
		if (a.Nom < b.Nom) {
			return -1;
		}
		if (a.Nom > b.Nom) {
			return 1;
		}
		return 0;
	});
	afficherEcran(ecranEleves);
}

function modifierEleve(id) {
	creationEleve["Nom"] = ecranModifEleveNom.value;
	creationEleve["Naissance"] = ecranModifEleveNaissance.value;
	donnees["eleves"].splice(id, 1, creationEleve)
	creationEleve = {"Nom":"","Naissance":"01/01/2024","Niveau":"","Competences":[],"Commentaires":[]};
	donnees["eleves"].sort((a, b) => {
		if (a.Nom < b.Nom) {
			return -1;
		}
		if (a.Nom > b.Nom) {
			return 1;
		}
		return 0;
	});
	genererDetailsEleve(id);
}

function supprimerEleve(id) {
	donnees["eleves"].splice(id, 1);
	afficherEcran(ecranEleves);
}

function modificationNiveau(id) {
	creationEleve["Niveau"] = id;
	modificationEleve('');
}

/* Import d'élèves */

function afficherImport() {
	afficherCache();
	let contenuHTML = '<h2>Importer des élèves</h2><p>Vous pouvez ici sélectionner un fichier issu de la fonction <em>Export d\'élève(s)</em> ou d\'un autre enregistrement de <em>Mon Livret Maternelle</em> pour fusionner leurs élèves à votre fichier.<br><br><strong>Attention</strong> : si les domaines et compétences renseignées pour les élèves ne correspondent pas aux vôtres elles n\'apparaitront plus dans le livret de l\'élève.</p><br><input type="file" accept=".json,application/json" onchange="verifImport(event)"/><br><br>';
	contenuHTML += '<div class="bFormation main bViolet" onclick="afficherEcran(ecranEleves)">Annuler</div>';
	boiteMulti.innerHTML = contenuHTML;
	boiteMulti.style.display='block';
}

function verifImport(event) {
	let file = event.target.files[0];
	if (file) {
		let reader = new FileReader();
		reader.onload = function (e) {
			let contenu = e.target.result;
			donneesImport = JSON.parse(contenu);
			traitementImport();
		};
		reader.readAsText(file);
	}
}

function traitementImport() {
	let contenuHTML = '<h2>Lecture du fichier</h2>';
	if (donneesImport && donneesImport["parametres"] && donneesImport["competences"] && donneesImport["eleves"]) { // Import depuis un fichier livret
		if (idImport.length == 0) {
			donneesImport["eleves"].forEach((el, i) => {
				idImport.push(i);
			});
		}
		contenuHTML += '<p>Import depuis un <strong>Fichier livret</strong>.</p><p class="gauche">Liste des élèves à importer<br><em>Vous pouvez cliquer sur les élèves pour les désélectionner</em></p><div class="grilleQuatre">';
		donneesImport["eleves"].forEach((el, i) => {
			if (idImport.indexOf(i) !== -1) {
				contenuHTML += '<div class="main borderOui" onclick="changerImportEleve(0, '+i+')">'+ el["Nom"] +'</div>'
			} else {
				contenuHTML += '<div class="main borderNon" onclick="changerImportEleve(1, '+i+')">'+ el["Nom"] +'</div>'
			}
		});
		contenuHTML += '</div><br><div class="bFormation bViolet main" onclick="annulerImportEleve()">Annuler</div><div class="bFormation bVert main" onclick="validerImportEleve(1)">Importer ces élèves</div>';
	} else if (donneesImport && donneesImport[0] && donneesImport[0]["Nom"] && donneesImport[0]["Naissance"] && donneesImport[0]["Niveau"] && donneesImport[0]["Competences"] && donneesImport[0]["Commentaires"]) { // Import depuis un fichier élèves
		if (idImport.length == 0) {
			donneesImport.forEach((el, i) => {
				idImport.push(i);
			});
		}
		contenuHTML += '<p>Import depuis un <strong>Fichier élèves</strong>.</p><p class="gauche">Liste des élèves à importer<br><em>Vous pouvez cliquer sur les élèves pour les désélectionner</em></p><div class="grilleQuatre">';
		donneesImport.forEach((el, i) => {
			if (idImport.indexOf(i) !== -1) {
				contenuHTML += '<div class="main borderOui" onclick="changerImportEleve(0, '+i+')">'+ el["Nom"] +'</div>'
			} else {
				contenuHTML += '<div class="main borderNon" onclick="changerImportEleve(1, '+i+')">'+ el["Nom"] +'</div>'
			}
		});
		contenuHTML += '</div><br><div class="bFormation bViolet main" onclick="annulerImportEleve()">Annuler</div><div class="bFormation bVert main" onclick="validerImportEleve(2)">Importer ces élèves</div>';
	} else { // Autre fichier JSON
		contenuHTML += 'Erreur, le fichier sélectionné ne semble pas correspondre à un fichier d\'export ou une sauvegarde de Mon Livret Maternelle. Merci de vérifier votre fichier avant de recommencer.<br><br><div class="bFormation bViolet main" onclick="annulerImportEleve()">Annuler</div>';
	}
	boiteMulti.innerHTML = contenuHTML;
}

function changerImportEleve(type, id) {
	if (type == 1) { // Ajout dans les import, type = 1
		idImport.push(id);
	} else { // Retrait des imports, type = 0
		idImport.splice(idImport.indexOf(id), 1);
	}
	traitementImport();
}

function annulerImportEleve() {
	afficherEcran(ecranEleves);
}

function validerImportEleve(type) {
	if (type == 1) { // Import depuis fichier livret
		idImport.forEach((el, i) => {
			let nvEleve = donneesImport["eleves"][el]
			donnees["eleves"].push(nvEleve);
		});
	} else if (type == 2) { // Import depuis fichier élève
		idImport.forEach((el, i) => {
			let nvEleve = donneesImport[el];
			donnees["eleves"].push(nvEleve);
		});
	}
	donnees["eleves"].sort((a, b) => {
		if (a.Nom < b.Nom) {
			return -1;
		}
		if (a.Nom > b.Nom) {
			return 1;
		}
		return 0;
	});
	afficherEcran(ecranEleves);
}

/* Export d'élèves */

function afficherExport() {
	afficherCache();
	let contenuHTML = '<h2>Exporter des élèves</h2><p>Vous pouvez ici sélectionner un ou plusieurs élèves pour créer un fichier d\'export. Ce fichier pourra être importé dans une autre sauvegarde de <em>Mon Livret Maternelle</em> en cas de changement de classe ou d\'école.<br><br>Élèves à exporter :</p><div class="grilleQuatre">';
	donnees["eleves"].forEach((el, i) => {
		if (idImport.indexOf(i) !== -1) {
			contenuHTML += '<div class="main borderOui" onclick="changerExportEleve(0, '+i+')">'+el["Nom"]+'</div>'
		} else {
			contenuHTML += '<div class="main borderNon" onclick="changerExportEleve(1, '+i+')">'+el["Nom"]+'</div>'
		}
	});
	contenuHTML += '</div><br><br><div class="bFormation main bViolet" onclick="afficherEcran(ecranEleves)">Annuler</div>';
	if (idImport.length > 0) {
		contenuHTML += '<div aria-label="Seules les données des élèves seront exportées, les compétences et paramètres de la classe ne seront pas dans le document." class="main bFormation bBleu hint--top hint--info hint--medium hint--rounded" onclick="validerExportEleves(1)">Exporter les élèves sélectionnés</div><div aria-label="Un fichier de sauvegarde sera créé avec les élèves sélectionnés, les compétences et les paramètres de la classe." class="main bFormation bVert hint--top hint--success hint--medium hint--rounded" onclick="validerExportEleves(2)">Créer une classe avec ces élèves</div>'
	}
	boiteMulti.innerHTML = contenuHTML;
	boiteMulti.style.display='block';
}

function changerExportEleve(type, id) {
	if (type == 1) { // Ajout dans les exports, type = 1
		idImport.push(id);
	} else { // Retrait des exports, type = 0
		idImport.splice(idImport.indexOf(id), 1);
	}
	afficherExport();
}

function validerExportEleves(type) {
	if (type == 1) { // Export simple des élèves
		let donneesExport = [];
		idImport.forEach((el, i) => {
			donneesExport.push(donnees["eleves"][el]);
		});
		donneesExport = JSON.stringify(donneesExport);
		let blob = new Blob([donneesExport], {
			type: "application/json;charset=utf-8"
		});
		let url = URL.createObjectURL(blob);
		let a = document.createElement("a");
		a.href = url;
		a.download = "export élèves - MonLivretMaternelle.json";
		a.click();
		URL.revokeObjectURL(url);
		afficherEcran(ecranEleves);
	} else if (type == 2) { // Création d'un fichier classe
		let donneesExport = JSON.parse(JSON.stringify(donnees));
		donneesExport["eleves"].splice(0, donneesExport["eleves"].length)
		idImport.forEach((el, i) => {
			donneesExport["eleves"].push(donnees["eleves"][el]);
		});
		donneesExport = JSON.stringify(donneesExport);
		let blob = new Blob([donneesExport], {
			type: "application/json;charset=utf-8"
		});
		let url = URL.createObjectURL(blob);
		let a = document.createElement("a");
		a.href = url;
		a.download = "nouvelle classe - MonLivretMaternelle.json";
		a.click();
		URL.revokeObjectURL(url);
		afficherEcran(ecranEleves);
	}

}

/* Changement d'année scolaire */

function afficherChangAnnee() {
	afficherCache();
	let contenuHTML = '<h2>Changement d\'année scolaire</h2><p>La procédure de changement d\'année scolaire vous permet de faire passer automatiquement les élèves de leur niveau actuel au niveau suivant.<br>Avant de procéder au changement d\'année, assurez-vous d\'avoir importer tous les élèves de l\'école dans le même fichier via la fonctionnalité d\'import d\'élève.</p><p class="gauche"><br>La procédure se dérouble en 4 étapes :<ol class="gauche"><li>Suppression des GS</li><li>Passage des MS en GS</li><li>Passage des PS en MS</li><li>Passage des TPS en PS</li></ol><br><p>Vous pourrez déselectionner des élèves à chaque étape en cas de redoublement.</p><br><div class="bFormation main bViolet" onclick="afficherEcran(ecranEleves)">Annuler</div><div class="bFormation main bVert" onclick="ChangAnnee(0)">Débuter la procédure</div>';
	boiteMulti.innerHTML = contenuHTML;
	boiteMulti.style.display = 'block';
}

function ChangAnnee(etape) {
	let contenuHTML = '';
	switch (etape) {
		case 0: //Préparation des GS
			donnees["eleves"].forEach((el, i) => {
				if (el["Niveau"] == "GS") {
					idImport.push(i);
				}
			});
			ChangAnnee(1);
		case 1: //Choix des élèves de GS à supprimer
			contenuHTML += '<h2>1. Suppression des GS</h2><p>Les élèves de GS vont quitter l\'école, nous allons donc supprimer leurs données. Assurez-vous d\'avoir imprimé leur carnet avant d\'effectuer cette étape. Une fois supprimées, les données ne pourront plus être récupérées.</p><p class="gauche">Liste des élèves de GS à supprimer<br><em>Vous pouvez déselectionner un élève en cliquant dessus.</em></p><div class="grilleQuatre">';
			donnees["eleves"].forEach((el, i) => {
				if (el["Niveau"] == 'GS' && idImport.indexOf(i) !== -1) {
					contenuHTML += '<div class="main borderOui" onclick="changerEleveAnnee(1, 1, '+i+')">'+ el["Nom"] +'</div>';
				} else if (el["Niveau"] == "GS") {
					contenuHTML += '<div class="main borderNon" onclick="changerEleveAnnee(1, 0, '+i+')">'+ el["Nom"] +'</div>';
				}
			});
			contenuHTML += '</div><div class="bFormation main bViolet" onclick="afficherEcran(ecranEleves)">Annuler</div><div class="bFormation bVert main" onclick="ChangAnnee(2)">Passer à l\'étape 2/4</div>'
			break;
		case 2: //Suppression des GS et préparation des MS
			idImport.sort((a, b) => b - a);
			idImport.forEach((el) => {
				donnees["eleves"].splice(el, 1);
			});
			idImport.splice(0, idImport.length);
			donnees["eleves"].forEach((el, i) => {
				if (el["Niveau"] == "MS") {
					idImport.push(i);
				}
			});
			ChangAnnee(3);
		case 3: //Choix des élèves de MS à passer en GS
			contenuHTML += '<h2>2. Passage des MS en GS</h2><p class="gauche">Liste des élèves de MS à passer en GS<br><em>Vous pouvez déselectionner un élève en cliquant dessus.</em></p><div class="grilleQuatre">'
			donnees["eleves"].forEach((el, i) => {
				if (el["Niveau"] == 'MS' && idImport.indexOf(i) !== -1) {
					contenuHTML += '<div class="main borderOui" onclick="changerEleveAnnee(3, 1, '+i+')">'+ el["Nom"] +'</div>';
				} else if (el["Niveau"] == "MS") {
					contenuHTML += '<div class="main borderNon" onclick="changerEleveAnnee(3, 0, '+i+')">'+ el["Nom"] +'</div>';
				}
			});
			contenuHTML += '</div><div class="bFormation main bViolet hint--top hint--medium hint--rounded" aria-label="Les changements effectués aux étapes précédentes ne seront pas annulés." onclick="afficherEcran(ecranEleves)">Annuler</div><div class="bFormation bVert main" onclick="ChangAnnee(4)">Passer à l\'étape 3/4</div>';
			break;
		case 4: //Passage des MS et préparation des TPS
			idImport.forEach((el) => {
				donnees["eleves"][el]["Niveau"] = "GS";
			});
			idImport.splice(0, idImport.length);
			donnees["eleves"].forEach((el, i) => {
				if (el["Niveau"] == "PS") {
					idImport.push(i);
				}
			});
			ChangAnnee(5);
		case 5: //Choix des élèves de PS à passer en MS
			contenuHTML += '<h2>3. Passage des PS en MS</h2><p class="gauche">Liste des élèves de PS à passer en MS<br><em>Vous pouvez déselectionner un élève en cliquant dessus.</em></p><div class="grilleQuatre">'
			donnees["eleves"].forEach((el, i) => {
				if (el["Niveau"] == 'PS' && idImport.indexOf(i) !== -1) {
					contenuHTML += '<div class="main borderOui" onclick="changerEleveAnnee(5, 1, '+i+')">'+ el["Nom"] +'</div>';
				} else if (el["Niveau"] == "PS") {
					contenuHTML += '<div class="main borderNon" onclick="changerEleveAnnee(5, 0, '+i+')">'+ el["Nom"] +'</div>';
				}
			});
			contenuHTML += '</div><div class="bFormation main bViolet hint--top hint--medium hint--rounded" aria-label="Les changements effectués aux étapes précédentes ne seront pas annulés." onclick="afficherEcran(ecranEleves)">Annuler</div><div class="bFormation bVert main" onclick="ChangAnnee(6)">Passer à l\'étape 4/4</div>';
			break;
		case 6: //Passage des PS et préparation des TPS
			idImport.forEach((el) => {
				donnees["eleves"][el]["Niveau"] = "MS";
			});
			idImport.splice(0, idImport.length);
			donnees["eleves"].forEach((el, i) => {
				if (el["Niveau"] == "TPS") {
					idImport.push(i);
				}
			});
			ChangAnnee(7);
		case 7:  //Choix des élèves de TPS à passer en PS
			contenuHTML += '<h2>4. Passage des TPS en PS</h2><p class="gauche">Liste des élèves de TPS à passer en PS<br><em>Vous pouvez déselectionner un élève en cliquant dessus.</em></p><div class="grilleQuatre">'
			donnees["eleves"].forEach((el, i) => {
				if (el["Niveau"] == 'TPS' && idImport.indexOf(i) !== -1) {
					contenuHTML += '<div class="main borderOui" onclick="changerEleveAnnee(7, 1, '+i+')">'+ el["Nom"] +'</div>';
				} else if (el["Niveau"] == "TPS") {
					contenuHTML += '<div class="main borderNon" onclick="changerEleveAnnee(7, 0, '+i+')">'+ el["Nom"] +'</div>';
				}
			});
			contenuHTML += '</div><div class="bFormation main bViolet hint--top hint--medium hint--rounded" aria-label="Les changements effectués aux étapes précédentes ne seront pas annulés." onclick="afficherEcran(ecranEleves)">Annuler</div><div class="bFormation bVert main" onclick="ChangAnnee(8)">Finaliser le changement d\'année</div>';
			break;
		case 8: //Passage des TPS et retour à la liste des élèves
			idImport.forEach((el) => {
				donnees["eleves"][el]["Niveau"] = "PS";
			});
			idImport.splice(0, idImport.length);
			afficherEcran(ecranEleves);
			break;
	}
	boiteMulti.innerHTML = contenuHTML;
}

function changerEleveAnnee(etape, type, id) {
	if (type == 0) { // type = 0 -> ajout de l'élève dans le passage
		idImport.push(id);
	} else { // type = 1 -> retrait de l'élève du passage
		idImport.splice(idImport.indexOf(id), 1);
	}
	ChangAnnee(etape);
}

/* Supression en lot d'élèves */

function suppressionEnLot() {
	afficherCache();
	let contenuHTML = '<h2>Suppression de plusieurs élèves</h2><p>Sélectionnez les élèves à supprimer</p><div class="grilleQuatre">';
	donnees["eleves"].forEach((el, i) => {
		if (idImport.indexOf(i) !== -1) {
			contenuHTML += '<div class="main borderOui" onclick="changSupprEleves(1, '+i+')">'+el["Nom"]+'</div>';
		} else {
			contenuHTML += '<div class="main borderNon" onclick="changSupprEleves(0, '+i+')">'+el["Nom"]+'</div>';
		}
	});
	contenuHTML += '</div>'
	if (idImport.length !== 0) {
		contenuHTML += '<br><div class="bFormation bViolet main" onclick="afficherEcran(ecranEleves)">Annuler</div><div class="bFormation bRouge main" onclick="validerSuppressionEnLot()">Supprimer les élèves sélectionnés</div>';
	} else {
		contenuHTML += '<br><div class="bFormation bViolet main" onclick="afficherEcran(ecranEleves)">Annuler</div>';
	}
	boiteMulti.innerHTML = contenuHTML;
	boiteMulti.style.display = 'block';
}

function changSupprEleves(type, id) {
	if (type == 0) { //Élève à ajouter à la suppression
		idImport.push(id);
	} else { //Élève à enlever de la suppression, type = 1
		idImport.splice(idImport.indexOf(id), 1);
	}
	suppressionEnLot();
}

function validerSuppressionEnLot() {
	idImport.sort((a, b) => b - a);
	idImport.forEach((el) => {
		donnees["eleves"].splice(el, 1);
	});
	afficherEcran(ecranEleves)
}

/* Affichage du livret de l'élève */

function genererDetailsEleve(id) {
	cacherTout();
	ecranDetailsEleve.style.display = 'block';
	let contenuHTML = '<div class="centrer yesPrint printPage introPrint"><h1>'+donnees["parametres"]["ecole"]+'</h1>';
	if (donnees["parametres"]["imageIllustration"] !== undefined) {
		contenuHTML += '<img src="'+donnees["parametres"]["imageIllustration"]+'"/><br>';
	}
	contenuHTML += '<p>Année scolaire : '+donnees["parametres"]["anneeScolaire"]+'</p><br><h2>'+donnees["parametres"]["classe"]+'</h2><br><p class="gauche"><strong>Introduction : </strong><br>'+donnees["parametres"]["motIntroduction"].replace(/\n/g, '<br>')+'</p></div>';
	contenuHTML += '<div class="grilleDuo barreEleve pageBreak"><div><h2>'+donnees["eleves"][id]["Nom"]+'</h2><em>'+donnees["eleves"][id]["Naissance"]+'</em><br><div class="etiquette inblo b'+donnees["eleves"][id]["Niveau"]+'">'+donnees["eleves"][id]["Niveau"]+'</div></div><div class="droite noPrint"><div class="bFormation main bBleu hint--bottom hint--rounded" aria-label="Modifier les informations de l\'élève" onclick="modificationEleve('+id+')"><img alt="Modifier" src="src/b-modifier.svg"/></div><div class="bFormation main bViolet hint--bottom hint--rounded" aria-label="Supprimer le dossier de l\'élève" onclick="supprimerEleve('+id+')"><img alt="Supprimer" src="src/b-supprimer.svg"/></div><br><div class="bFormation main bJaune" onclick="window.print()">Imprimer le livret</div></div></div><br><h2 class="noPrint">Compétences</h2><p class="noPrint">Accès rapide :</p>';
	donnees["competences"].forEach((domaine, i1) => {
		contenuHTML += '<a href="#domaineE'+i1+'"><div class="bFormation bGris noPrint">'+domaine["domaine"]+'</div></a>';
	});
	contenuHTML += '<a href="#commentaires"><div class="bFormation main noPrint bJaune">Commentaires généraux</div></a>';
	donnees["competences"].forEach((domaine, i1) => {
		contenuHTML += '<div id="domaineE'+i1+'" class="boite printPage"><h2 style="color:'+domaine["couleur"]+';">'+domaine["domaine"]+'</h2><em>'+domaine["motIntro"].replaceAll('\n', '<br>')+'</em><div class="grilleComp">';
		domaine["competences"].forEach((compDom, i2) => {
			let estAffichee = false;
			let contenuCommentaires = '';
			donnees["eleves"][id]["Competences"].forEach((compVerif,i3) => {
				if (compVerif["domaine"] === donnees["competences"][i1]["domaine"] && compVerif["nom"] === donnees["competences"][i1]["competences"][i2]["nom"]) {
					contenuCommentaires += '<div class="comComp main" onclick="modifierCompEleve('+id+','+i3+')"><div class="etiquette inblo b'+compVerif["niveau"]+'">'+compVerif["niveau"]+'</div> '+compVerif["date"]+'<br><em>'+compVerif["commentaire"]+'</em></div>';
					estAffichee = true;
				}
			});
			if (estAffichee === false) {
				contenuHTML += '<div class="competence boite centrer noPrint"><img src="'+compDom["image"]+'"/><p>'+compDom["nom"]+'</p>';
			} else {
				contenuHTML += '<div class="competence boite centrer"><img src="'+compDom["image"]+'"/><p>'+compDom["nom"]+'</p>'+contenuCommentaires+'';
			}
			
			contenuHTML += '<div class="bFormation addComp main bGris noPrint hint--bottom hint--rounded" aria-label="Ajouter la compétence au carnet de l\'élève" onclick="ajouterCompEleve('+id+','+i1+','+i2+')"><img alt="Ajouter" src="src/b-ajouter.svg"/></div></div>';
		});
		contenuHTML += '</div><div class="alignRight noPrint"><div class="bFormation main bBleu hint--bottom hint--rounded" aria-label="Ajouter une photo" onclick="ajouterImageEleve('+id+','+i1+')"><img alt="ajouter une photo" src="src/b-photo.svg" /></div></div><div class="grillePhoto">';
		if (donnees["eleves"][id]["Images"]) {
			donnees["eleves"][id]["Images"].forEach((imgVerif,i4) => {
				if (imgVerif["domaine"] === donnees["competences"][i1]["domaine"]) {
					contenuHTML += '<div class="photo boite centrer main" onclick="modifierImageEleve('+id+','+i4+')"><img src="'+imgVerif["image"]+'"/>';
					if (imgVerif["image"] !== undefined || imgVerif["image"] !== '') {
						contenuHTML += '<p>'+imgVerif["commentaire"]+'</p>';
					}
					contenuHTML += '</div>';
				}
			});
		}
		contenuHTML += '</div><div class="alignRight noPrint"><div class="bFormation main bVert hint--bottom hint--rounded" aria-label="Ajouter un commentaire au domaine" onclick="ajouterComDansDomaine('+id+','+i1+')"><img alt="Ajouter un commentaire au domaine" src="src/b-commentaire.svg"/></div></div><div>';
		if (donnees["eleves"][id]["CommentairesDomaine"]) {
			donnees["eleves"][id]["CommentairesDomaine"].forEach((el, i5) => {
				if (el["domaine"] == domaine["domaine"]) {
					contenuHTML += '<div class="comGen main" onclick="modifierComDansDomaine('+id+','+i5+')"><div class="etiquette inblo b'+el["niveau"]+'">'+el["niveau"]+'</div> '+el["date"]+'<br><p>'+el["contenu"].replaceAll('\n', '<br>')+'</p></div>';
				}
			});
		}
		contenuHTML += '</div></div><br>';
	});
	contenuHTML += '<div class="boite"><div class="grilleDuo21"><div><h2 id="commentaires">Commentaires généraux</h2></div><div class="droite noPrint"><div class="bFormation main bVert noPrint hint--bottom hint--rounded" aria-label="Ajouter un commentaire" onclick="ajouterCommentaireEleve('+id+')"><img alt="Ajouter un commentaire" src="src/b-commentaire.svg"/></div></div></div>';
	donnees["eleves"][id]["Commentaires"].forEach((elComm, iCom) => {
		contenuHTML += '<div class="comGen main" onclick="modifierCommentaireEleve('+id+','+iCom+')"><div class="etiquette inblo b'+elComm["niveau"]+'">'+elComm["niveau"]+'</div> '+elComm["date"]+'<br><p>'+elComm["contenu"].replaceAll('\n', '<br>')+'</p></div>';
	});
	contenuHTML += '</div><div class="barreNav noPrint">';
	if (id > 0) {
		let idPrev = id - 1;
		contenuHTML += '<div class="main gauche" onclick="genererDetailsEleve('+idPrev+')">< élève précédent</div>';
	} else {
		contenuHTML += '<div></div>';
	}
	contenuHTML += '<div class="centrer main" onclick="retourHaut()"> Haut de page</div>';
	if (id < donnees["eleves"].length -1) {
		let idNext = id + 1;
		contenuHTML += '<div class="main droite" onclick="genererDetailsEleve('+idNext+')">élève suivant ></div>';
	} else {
		contenuHTML += '<div></div>';
	}
	contenuHTML += '</div>';
	ecranDetailsEleve.innerHTML = contenuHTML;
}

function retourHaut() {
	window.scrollTo({
		top: 0,
		behavior: 'smooth'
	});
}

/* Gestion des commentaires dans les domaines */

function ajouterComDansDomaine(eleve, domaine) {
	afficherCache();
	ecranCommentaireDomaine.style.display = 'block';
	ecranCommentaireDomaineTitre.innerHTML = 'Ajouter un commentaire de domaine<br><em>'+donnees["competences"][domaine]["domaine"]+'</em>';
	ecranCommentaireDomaineDate.value = formattedDate;
	ecranCommentaireDomaineContenu.value = '';
	ecranCommentaireDomaineBoutons.innerHTML = '<div class="bFormation main bBleu" onclick="genererDetailsEleve('+eleve+')">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderAjouterCommentaireDomaine('+eleve+','+domaine+')">Valider</div>';
}

function sauvegarderAjouterCommentaireDomaine(id, domaine) {
	let nouveauCommentaire = {
		"niveau":donnees["eleves"][id]["Niveau"],
		"date":ecranCommentaireDomaineDate.value,
		"domaine":donnees["competences"][domaine]["domaine"],
		"contenu":ecranCommentaireDomaineContenu.value,
	};
	if (!donnees["eleves"][id]["CommentairesDomaine"]) {
		donnees["eleves"][id]["CommentairesDomaine"] = [];
	}
	donnees["eleves"][id]["CommentairesDomaine"].push(nouveauCommentaire);
	genererDetailsEleve(id);
}

function modifierComDansDomaine(eleve, commentaire) {
	afficherCache();
	ecranCommentaireDomaine.style.display = 'block';
	ecranCommentaireDomaineTitre.innerHTML = 'Modifier le commentaire de domaine<br><em>'+donnees["eleves"][eleve]["CommentairesDomaine"][commentaire]["domaine"]+'</em>';
	ecranCommentaireDomaineDate.value = donnees["eleves"][eleve]["CommentairesDomaine"][commentaire]["date"];
	ecranCommentaireDomaineContenu.value = donnees["eleves"][eleve]["CommentairesDomaine"][commentaire]["contenu"];
	ecranCommentaireDomaineBoutons.innerHTML = '<div class="bFormation main bViolet" onclick="supprimerComDansDomaine('+eleve+','+commentaire+')">Supprimer</div><div class="bFormation main bBleu" onclick="genererDetailsEleve('+eleve+')">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderModifierCommentaireDomaine('+eleve+','+commentaire+')">Valider</div>';
}

function sauvegarderModifierCommentaireDomaine(eleve, commentaire) {
	donnees["eleves"][eleve]["CommentairesDomaine"][commentaire]["date"] = ecranCommentaireDomaineDate.value;
	donnees["eleves"][eleve]["CommentairesDomaine"][commentaire]["contenu"] = ecranCommentaireDomaineContenu.value;
	genererDetailsEleve(eleve);
}

function supprimerComDansDomaine(eleve, commentaire) {
	donnees["eleves"][eleve]["CommentairesDomaine"].splice(commentaire, 1)
	genererDetailsEleve(eleve);
}

/* Gestion des photos d'illustration des livrets élèves */

function ajouterImageEleve(id, i1) {
	afficherCache();
	ecranAjoutImage.style.display = 'block';
	ecranAjoutImageTitre.innerHTML = 'Ajouter une photographie<br><em>'+donnees["competences"][i1]["domaine"]+'</em>';
	if (donnees["eleves"][id]["Images"] == undefined) {
		donnees["eleves"][id]["Images"] = [];
	}
	photoActuelle = {"domaine":donnees["competences"][i1]["domaine"],"image":"","commentaire":""};
	ecranAjoutImageSource.value = '';
	ecranAjoutImageCommentaire.value = photoActuelle["commentaire"];
	outputPhoto.innerHTML = '';
	ecranAjoutImageBoutons.innerHTML = '<div class="bFormation bBleu main" onclick="genererDetailsEleve('+id+')">Annuler</div><div class="bFormation bVert main" onclick="enregistrerNvPhoto('+id+')">Valider</div>';
}

function commentairePhotoEleve(event) {
	photoActuelle["commentaire"] = event.target.value;
}

function enregistrerNvPhoto(id) {
	let image = JSON.parse(JSON.stringify(photoActuelle));
	donnees["eleves"][id]["Images"].push(image);
	genererDetailsEleve(id);
}

function modifierImageEleve(eleve, image) {
	afficherCache();
	ecranAjoutImage.style.display = 'block';
	ecranAjoutImageTitre.innerHTML = 'Modifier une photographie<br><em>'+donnees["eleves"][eleve]["Images"][image]["domaine"]+'</em>';
	photoActuelle = JSON.parse(JSON.stringify(donnees["eleves"][eleve]["Images"][image]));
	ecranAjoutImageSource.value = '';
	ecranAjoutImageCommentaire.value = photoActuelle["commentaire"];
	outputPhoto.innerHTML = '<img src="'+photoActuelle["image"]+'"/>';
	ecranAjoutImageBoutons.innerHTML = '<div class="bFormation bViolet main" onclick="supprimerPhotoEleve('+eleve+','+image+')">Supprimer</div><div class="bFormation bBleu main" onclick="genererDetailsEleve('+eleve+')">Annuler</div><div class="bFormation bVert main" onclick="enregistrerModifPhoto('+eleve+','+image+')">Valider</div>';
}

function enregistrerModifPhoto(eleve, image) {
	donnees["eleves"][eleve]["Images"][image] = JSON.parse(JSON.stringify(photoActuelle));
	genererDetailsEleve(eleve);
}

function supprimerPhotoEleve(eleve, image) {
	donnees["eleves"][eleve]["Images"].splice(image, 1)
	genererDetailsEleve(eleve);
}

function photo2base64(event) {
    let file = event.target.files[0];
    if (file) {
        let reader = new FileReader();
        reader.onload = function(e) {
            let img = new Image();
            img.onload = function() {
                let width = img.width;
                let height = img.height;
                // Check if the image needs to be resized
                if (width > 450) {
                    height = Math.round((450 / width) * height);
                    width = 450;
				}
				if (height > 300) {
					width = Math.round((300 / height) * width);
					height = 300;
				}
                // Create a canvas to draw the resized image
                const canvas = document.createElement('canvas');
                canvas.width = width;
                canvas.height = height;
                const ctx = canvas.getContext('2d');
                ctx.drawImage(img, 0, 0, width, height);
                // Convert the resized image to Base64
                const base64String = canvas.toDataURL('image/jpeg');
                outputPhoto.innerHTML = `<img src="${base64String}">`;
				photoActuelle["image"] = base64String;
            };
            img.src = e.target.result;
        };
        reader.readAsDataURL(file);
    }
}



/* Gestion des commentaires des livrets élèves */

function ajouterCommentaireEleve(id) {
	afficherCache();
	ecranCommentaireEleveTitre.innerHTML = 'Ajouter un commentaire<br><em>'+donnees["eleves"][id]["Nom"]+' - <div class="etiquette inblo b'+donnees["eleves"][id]["Niveau"]+'">'+donnees["eleves"][id]["Niveau"]+'</div></em>';
	ecranCommentaireEleveDate.value = formattedDate;
	ecranCommentaireEleveContenu.value = '';
	ecranCommentaireEleveBoutons.innerHTML = '<div class="bFormation main bBleu" onclick="genererDetailsEleve('+id+')">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderAjouterCommentaireEleve('+id+')">Valider</div>';
	ecranCommentaireEleve.style.display = 'block';
}

function modifierCommentaireEleve(iEl, iCo) {
	afficherCache();
	ecranCommentaireEleveTitre.innerHTML = 'Modifier un commentaire<br><em>'+donnees["eleves"][iEl]["Nom"]+' - <div class="etiquette inblo b'+donnees["eleves"][iEl]["Commentaires"][iCo]["Niveau"]+'">'+donnees["eleves"][iEl]["Commentaires"][iCo]["Niveau"]+'</div></em>';
	ecranCommentaireEleveDate.value = donnees["eleves"][iEl]["Commentaires"][iCo]["date"];
	ecranCommentaireEleveContenu.value = donnees["eleves"][iEl]["Commentaires"][iCo]["contenu"];
	ecranCommentaireEleveBoutons.innerHTML = '<div class="bFormation main bViolet" onclick="supprimerCommentaireEleve('+iEl+','+iCo+')">Supprimer</div><div class="bFormation main bBleu" onclick="genererDetailsEleve('+iEl+')">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderModifierCommentaireEleve('+iEl+','+iCo+')">Valider</div>';
	ecranCommentaireEleve.style.display = 'block';
}

function sauvegarderAjouterCommentaireEleve(id) {
	let nouveauCommentaire = {
		"niveau":donnees["eleves"][id]["Niveau"],
		"date":ecranCommentaireEleveDate.value,
		"contenu":ecranCommentaireEleveContenu.value,
	};
	donnees["eleves"][id]["Commentaires"].push(nouveauCommentaire);
	genererDetailsEleve(id);
}

function sauvegarderModifierCommentaireEleve(iEl, iCo) {
	donnees["eleves"][iEl]["Commentaires"][iCo]["date"] = ecranCommentaireEleveDate.value;
	donnees["eleves"][iEl]["Commentaires"][iCo]["contenu"] = ecranCommentaireEleveContenu.value;
	genererDetailsEleve(iEl);
}

function supprimerCommentaireEleve(iEl, iCo) {
	donnees["eleves"][iEl]["Commentaires"].splice(iCo, 1);
	genererDetailsEleve(iEl);
}

/* Compétences des livrets élèves */

function ajouterCompEleve(eleve, domaine, comp) {
	afficherCache();
	ecranDetailEleveCompTitre.innerHTML = 'Ajouter la compétence';
	ecranDetailEleveCompDétails.innerHTML = 'Domaine : '+donnees["competences"][domaine]["domaine"]+'<br>Compétence : '+donnees["competences"][domaine]["competences"][comp]["nom"]+'<br><br>Élève : '+donnees["eleves"][eleve]["Nom"]+' <div class="etiquette inblo b'+donnees["eleves"][eleve]["Niveau"]+'">'+donnees["eleves"][eleve]["Niveau"]+'</div><br><br>';
	ecranDetailEleveCompDate.value = formattedDate;
	ecranDetailEleveCompCommentaire.value = '';
	ecranDetailEleveCompBoutons.innerHTML = '<div class="bFormation main bBleu" onclick="genererDetailsEleve('+eleve+')">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderAjouterCompEleve('+eleve+','+domaine+','+comp+')">Valider</div>';
	ecranDetailEleveComp.style.display = 'block';
}

function modifierCompEleve(eleve, comp) {
	afficherCache();
	ecranDetailEleveCompTitre.innerHTML = 'Modifier la compétence';
	ecranDetailEleveCompDétails.innerHTML = 'Domaine : '+donnees["eleves"][eleve]["Competences"][comp]["domaine"]+'<br>Compétence : '+donnees["eleves"][eleve]["Competences"][comp]["nom"]+'<br><br>Élève : '+donnees["eleves"][eleve]["Nom"]+' <div class="etiquette inblo b'+donnees["eleves"][eleve]["Niveau"]+'">'+donnees["eleves"][eleve]["Niveau"]+'</div><br><br>';
	ecranDetailEleveCompDate.value = donnees["eleves"][eleve]["Competences"][comp]["date"];
	ecranDetailEleveCompCommentaire.value = donnees["eleves"][eleve]["Competences"][comp]["commentaire"];
	ecranDetailEleveCompBoutons.innerHTML = '<div class="grilleDuo"><div><div class="bFormation main bViolet" onclick="sauvegarderSupprimerCompEleve('+eleve+','+comp+')">Supprimer</div></div><div><div class="bFormation main bBleu" onclick="genererDetailsEleve('+eleve+')">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderModifierCompEleve('+eleve+','+comp+')">Valider</div></div></div>';
	ecranDetailEleveComp.style.display = 'block';
}

function sauvegarderAjouterCompEleve(eleve, domaine, comp) {
	let nouvelleCompetence = {
		"domaine":donnees["competences"][domaine]["domaine"],
		"nom":donnees["competences"][domaine]["competences"][comp]["nom"],
		"niveau":donnees["eleves"][eleve]["Niveau"],
		"date":ecranDetailEleveCompDate.value,
		"commentaire":ecranDetailEleveCompCommentaire.value
	};
	donnees["eleves"][eleve]["Competences"].push(nouvelleCompetence);
	genererDetailsEleve(eleve);
}

function sauvegarderModifierCompEleve(eleve,comp) {
	donnees["eleves"][eleve]["Competences"][comp]["date"] = ecranDetailEleveCompDate.value;
	donnees["eleves"][eleve]["Competences"][comp]["commentaire"] = ecranDetailEleveCompCommentaire.value;
	genererDetailsEleve(eleve);
}

function sauvegarderSupprimerCompEleve(eleve,comp) {
	donnees["eleves"][eleve]["Competences"].splice(comp, 1);
	genererDetailsEleve(eleve);
}

/* Domaines d'enseignement */

function genererEcranCompetences() {
	let contenuHTML = '<div class="droite"><div class="bFormation main bVert" onclick="ajouterDomaine()">Ajouter un domaine</div></div><p>Accès rapide :</p>';
	donnees["competences"].forEach((domaine, i1) => {
		contenuHTML += '<a href="#domaine'+i1+'"><div class="bFormation bGris">'+domaine["domaine"]+'</div></a>';
	});
	donnees["competences"].forEach((domaine, i1) => {
		contenuHTML += '<div id="domaine'+i1+'" class="boite"><div class="grilleDuo21"><div><h2 style="color:'+domaine["couleur"]+';">'+domaine["domaine"]+'</h2><em>'+domaine["motIntro"].replaceAll('\n', '<br>')+'</em></div><div class="droite"><div class="bFormation main bBleu hint--bottom hint--rounded" aria-label="Modifier le domaine" onclick="modifierDomaine('+i1+')"><img alt="Modifier le domaine" src="src/b-modifier.svg" /></div><div class="bFormation main bViolet hint--bottom hint--rounded" aria-label="Supprimer le domaine" onclick="supprimerDomaine('+i1+')"><img alt="Supprimer le domaine" src="src/b-supprimer.svg" /></div>';
		if (i1 > 0) {
			contenuHTML += '<div class="bFormation bGris main hint--bottom hint--rounded" aria-label="Déplacer le domaine vers le haut" onclick="domaineUp('+i1+')"><img alt="Remonter" src="src/b-up.svg" /></div>';
		}
		if (i1 < donnees["competences"].length -1) {
			contenuHTML += '<div class="bFormation bGris main hint--bottom hint--rounded" aria-label="Déplacer le domaine vers le bas" onclick="domaineDown('+i1+')"><img alt="Descendre" src="src/b-down.svg" /></div>';
		}
		contenuHTML += '<br><div class="bFormation main bVert" onclick="ajouterCompetence('+i1+')">Ajouter une compétence</div></div></div><br><div class="grilleComp">';
		
		domaine["competences"].forEach((compDom, i2) => {
			contenuHTML += '<div class="competence boite centrer"><img src="'+compDom["image"]+'"/><p>'+compDom["nom"]+'</p><br>';
			contenuHTML += '<div class="bFormation main bBleu hint--bottom hint--rounded" aria-label="Modifier la compétence" onclick="modifierCompetence('+i1+','+i2+')"><img alt="Modifier" src="src/b-modifier.svg"/></div><div class="bFormation main bViolet hint--bottom hint--rounded" aria-label="Supprimer la compétence" onclick="supprimerCompetence('+i1+','+i2+')"><img alt="Supprimer" src="src/b-supprimer.svg"/></div><br>';
			if (i2 > 0) {
				contenuHTML += '<div class="bFormation main bGris hint--bottom hint--rounded" aria-label="Remonter la compétence dans la liste" onclick="competenceUp('+i1+','+i2+')"><img alt="Remonter la compétence" src="src/b-prev.svg"/></div>';
			}
			contenuHTML += '<div class="bFormation main bGris hint--bottom hint--rounded" aria-label="Déplacer la compétence vers un autre domaine" onclick="deplacerComp('+i1+','+i2+')"><img src="src/b-move.svg" alt="Déplacer"/></div>';
			if (i2 < domaine["competences"].length-1) {
				contenuHTML += '<div class="bFormation main bGris hint--bottom hint--rounded" aria-label="Descendre la compétence dans la liste" onclick="competenceDown('+i1+','+i2+')"><img alt="Descendre la compétence" src="src/b-next.svg" /></div>';
			}
			contenuHTML += '</div>';
		});
		contenuHTML += '</div></div>';
	});
	contenuHTML += '<br><br><div class="centrer">Fonctions avancées :<br><div class="main bFormation bGris" onclick="afficherImportModeleComp()">Importer un modèle de compétences</div><div class="main bFormation bGris hint--rounded hint--top hint--medium" aria-label="Seuls vos domaines et compétences seront exportés, pas les données de vos élèves." onclick="exportModeleComp()">Exporter un modèle de compétences</div></div>';
	listeCompetences.innerHTML = contenuHTML;
}

function domaineUp(id) {
	[donnees["competences"][id -1], donnees["competences"][id]] = [donnees["competences"][id], donnees["competences"][id-1]];
	genererEcranCompetences();
}

function domaineDown(id) {
	[donnees["competences"][id +1], donnees["competences"][id]] = [donnees["competences"][id], donnees["competences"][id+1]];
	genererEcranCompetences();
}

function ajouterDomaine() {
	afficherCache();
	ecranModifDomaine.style.display = 'block';
	ecranModifDomaineTitre.innerHTML = 'Ajouter un domaine';
	ecranModifDomaineNom.value = '';
	ecranModifDomaineCouleur.value = '#000000';
	ecranModifDomaineIntro.value = '';
	ecranModifDomaineBoutons.innerHTML = '<div class="bFormation main bViolet" onclick="afficherEcran(ecranCompetences)">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderAjouterDomaine()">Valider</div>';
}

function sauvegarderAjouterDomaine() {
	let nouveauDomaine = {
			"domaine":ecranModifDomaineNom.value,
			"motIntro":ecranModifDomaineIntro.value,
			"competences":[],
			"couleur":ecranModifDomaineCouleur.value
		};
	donnees["competences"].push(nouveauDomaine);
	afficherEcran(ecranCompetences);
}

function modifierDomaine(id) {
	afficherCache();
	ecranModifDomaine.style.display = 'block';
	ecranModifDomaineTitre.innerHTML = 'Modifier un domaine';
	ecranModifDomaineNom.value = donnees["competences"][id]["domaine"];
	ecranModifDomaineCouleur.value = donnees["competences"][id]["couleur"];
	ecranModifDomaineIntro.value = donnees["competences"][id]["motIntro"];
	ecranModifDomaineBoutons.innerHTML = '<div class="bFormation main bViolet" onclick="afficherEcran(ecranCompetences)">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderModifierDomaine('+id+')">Valider</div>';
}

function modifierDomaineDansEleves(avant, apres) {
	if (avant != apres) {
		donnees["eleves"].forEach((eleve) => {
			eleve["Competences"].forEach((element) => {
				element["domaine"] = element["domaine"].replaceAll(avant,apres);
			});
			if (eleve["Images"] !== undefined) {
				eleve["Images"].forEach((element) => {
					element["domaine"] = element["domaine"].replaceAll(avant,apres);
				});
			}
			if (eleve["CommentairesDomaine"] !== undefined) {
				eleve["CommentairesDomaine"].forEach((element) => {
					element["domaine"] = element["domaine"].replaceAll(avant,apres);
				});
			}
		});
	}
}

function sauvegarderModifierDomaine(id) {
	modifierDomaineDansEleves(donnees["competences"][id]["domaine"],ecranModifDomaineNom.value);
	donnees["competences"][id]["domaine"] = ecranModifDomaineNom.value;
	donnees["competences"][id]["couleur"] = ecranModifDomaineCouleur.value;
	donnees["competences"][id]["motIntro"] = ecranModifDomaineIntro.value;
	afficherEcran(ecranCompetences);
}

function supprimerDomaine(id) {
	donnees["eleves"].forEach((eleve, i1) => {
		if (eleve["Competences"]) {
			for (let i2 = eleve["Competences"].length - 1 ; i2 >= 0 ; i2--) {
				if (eleve["Competences"][i2]["domaine"] == donnees["competences"][id]["domaine"]) {
					eleve["Competences"].splice(i2, 1);
				}
			}
		}
		if (eleve["Images"]) {
			for (let i2 = eleve["Images"].length - 1 ; i2 >= 0 ; i2--) {
				if (eleve["Images"][i2]["domaine"] == donnees["competences"][id]["domaine"]) {
					eleve["Images"].splice(i2, 1);
				}
			}
		}
		if (eleve["CommentairesDomaine"]) {
			for (let i2 = eleve["CommentairesDomaine"].length - 1 ; i2 >= 0 ; i2--) {
				if (eleve["CommentairesDomaine"][i2]["domaine"] == donnees["competences"][id]["domaine"]) {
					eleve["CommentairesDomaine"].splice(i2, 1);
				}
			}
		}
	});
	donnees["competences"].splice(id, 1);
	afficherEcran(ecranCompetences);
}

ecranModifCompetenceImage.addEventListener('change', function(event) {
    const file = event.target.files[0];

    if (file) {
        const reader = new FileReader();

        reader.onload = function(e) {
            const img = new Image();

            img.onload = function() {
                let width = img.width;
                let height = img.height;

                // Check if the image needs to be resized
                if (width > 300 || height > 300) {
                    if (width > height) {
                        height = Math.round((300 / width) * height);
                        width = 300;
                    } else {
                        width = Math.round((300 / height) * width);
                        height = 300;
                    }
                }

                // Create a canvas to draw the resized image
                const canvas = document.createElement('canvas');
                canvas.width = width;
                canvas.height = height;
                const ctx = canvas.getContext('2d');
                ctx.drawImage(img, 0, 0, width, height);

                // Convert the resized image to Base64
                const base64String = canvas.toDataURL('image/jpeg');
                output.innerHTML = `<p>Image choisie :<br><img src="${base64String}"></p>`;
				imageBase64 = base64String;
            };

            img.src = e.target.result;
        };

        reader.readAsDataURL(file);
    }
});

/* Import et export de modèle de compétences */

function afficherImportModeleComp(origine) {
	afficherCache();
	let contenuHTML = '<h2>Importer un modèle de compétence</h2><p>L\'import de modèle de compétence permet de remplacer les domaines et compétences actuels par ceux d\'un fichier modèle.<br><em>Attention : si vous avez des éléments déjà inscrits dans les livrets élèves, ils ne seront plus visibles dans les livrets élèves si les domaines et compétences diffèrent de votre précédente configuration.</em></p><br><input type="file" accept=".json,application/json" onchange="verifImportModeleComp(event)"/><br><br>';
	if (origine == "accueil") {
		contenuHTML += '<div class="bFormation main bViolet" onclick="afficherEcran(ecranAccueil)">Annuler</div>';
	} else {
		contenuHTML += '<div class="bFormation main bViolet" onclick="afficherEcran(ecranCompetences)">Annuler</div>';
	}
	boiteMulti.innerHTML = contenuHTML;
	boiteMulti.style.display = 'block';
}

function verifImportModeleComp(event) {
	let file = event.target.files[0];
	if (file) {
		let reader = new FileReader();
		reader.onload = function (e) {
			let contenu = e.target.result;
			donneesImport = JSON.parse(contenu);
			traitementImportModeleComp();
		};
		reader.readAsText(file);
	}
}

function traitementImportModeleComp() {
	let contenuHTML = '<h2>Lecture du fichier</h2>';
	if (donneesImport && donneesImport["parametres"] && donneesImport["competences"] && donneesImport["eleves"]) { // Import depuis un fichier livret
		donnees["competences"] = JSON.parse(JSON.stringify(donneesImport["competences"]));
		afficherEcran(ecranCompetences);
	} else if (donneesImport && donneesImport[0] && donneesImport[0]["domaine"] && donneesImport[0]["competences"]) { // Import depuis un fichier modèle
		donnees["competences"] = JSON.parse(JSON.stringify(donneesImport));
		afficherEcran(ecranCompetences);
	} else { // Autre fichier JSON
		contenuHTML += 'Erreur, le fichier sélectionné ne semble pas correspondre à un fichier de sauvegarde de Mon Livret Maternelle ou à un fichier de modèle de compétences. Merci de vérifier votre fichier avant de recommencer.<br><br><div class="bFormation bViolet main" onclick="afficherEcran(ecranCompetences)">Annuler</div>'
	}
	boiteMulti.innerHTML = contenuHTML;
}

function exportModeleComp() {
	let contenuExport = JSON.stringify(donnees["competences"]);
	let blob = new Blob([contenuExport], {
		type: "application/json;charset=utf-8"
	});
	let url = URL.createObjectURL(blob);
	let a = document.createElement("a");
	a.href = url;
	a.download = "Modèle Compétence.json";
	a.click();
	URL.revokeObjectURL(url);
}

/* Gestion des compétences générales */

function ajouterCompetence(id) {
	afficherCache();
	ecranModifCompetenceTitre.innerHTML = 'Ajouter une compétence au domaine :<br><em>'+donnees["competences"][id]["domaine"]+'</em>';
	ecranModifCompetenceNom.value = '';
	ecranModifCompetenceImage.value = '';
	imageBase64 = '';
	output.innerHTML = '';
	ecranModifCompetenceBoutons.innerHTML = '<div class="bFormation main bViolet" onclick="afficherEcran(ecranCompetences)">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderAjouterCompetence('+id+')">Valider</div>';
	ecranModifCompetence.style.display = 'block';
}

function modifierCompetence(i1,i2) {
	afficherCache();
	ecranModifCompetenceTitre.innerHTML = 'Modifier une compétence du domaine :<br><em>'+donnees["competences"][i1]["domaine"]+'</em>';
	ecranModifCompetenceNom.value = donnees["competences"][i1]["competences"][i2]["nom"];
	ecranModifCompetenceImage.value = '';
	imageBase64 = donnees["competences"][i1]["competences"][i2]["image"];
	output.innerHTML = '<p>Image choisie :<br><img src="'+imageBase64+'"></p>';
	ecranModifCompetenceBoutons.innerHTML = '<div class="bFormation main bViolet" onclick="afficherEcran(ecranCompetences)">Annuler</div><div class="bFormation main bVert" onclick="sauvegarderModifierCompetence('+i1+','+i2+')">Valider</div>';
	ecranModifCompetence.style.display = 'block';
}

function modifierCompetenceDansEleves(avant, apres, domaine) {
	if (avant != apres) {
		donnees["eleves"].forEach((eleve) => {
			eleve["Competences"].forEach((element) => {
				if (element["domaine"] === donnees["competences"][domaine]["domaine"]) {
					element["nom"] = element["nom"].replaceAll(avant,apres);
				}
			});
		});
	}
}

function sauvegarderAjouterCompetence(id) {
	let nouvelleCompetence = {
			"nom":ecranModifCompetenceNom.value,
			"image":imageBase64
		};
	donnees["competences"][id]["competences"].push(nouvelleCompetence);
	afficherEcran(ecranCompetences);
}

function sauvegarderModifierCompetence(i1,i2) {
	modifierCompetenceDansEleves(donnees["competences"][i1]["competences"][i2]["nom"], ecranModifCompetenceNom.value, i1);
	donnees["competences"][i1]["competences"][i2]["nom"] = ecranModifCompetenceNom.value;
	donnees["competences"][i1]["competences"][i2]["image"] = imageBase64;
	afficherEcran(ecranCompetences);
}

function supprimerCompetence(i1, i2) {
	donnees["eleves"].forEach((eleve, i3) => {
		if (eleve["Competences"]) {
			for (let i4 = eleve["Competences"].length - 1 ; i4 >= 0 ; i4--) {
				if (eleve["Competences"][i4]["domaine"] == donnees["competences"][i1]["domaine"] && eleve["Competences"][i4]["nom"] == donnees["competences"][i1]["competences"][i2]["nom"]) {
					eleve["Competences"].splice(i4, 1);
				}
			}
		}
	});
	donnees["competences"][i1]["competences"].splice(i2, 1);
	afficherEcran(ecranCompetences);
}

function competenceUp(domaine, competence) {
	[donnees["competences"][domaine]["competences"][competence-1], donnees["competences"][domaine]["competences"][competence]] = [donnees["competences"][domaine]["competences"][competence], donnees["competences"][domaine]["competences"][competence-1]];
	genererEcranCompetences();
}

function competenceDown(domaine, competence) {
	[donnees["competences"][domaine]["competences"][competence+1], donnees["competences"][domaine]["competences"][competence]] = [donnees["competences"][domaine]["competences"][competence], donnees["competences"][domaine]["competences"][competence+1]];
	genererEcranCompetences();
}

function deplacerComp(domaine, competence) {
	afficherCache();
	let contenuHTML = '<div class="boiteFlottante"><h2>Déplacer la compétence vers un autre domaine</h2><p>Compétence à déplacer :<br><em>'+ donnees["competences"][domaine]["competences"][competence]["nom"] +'</em></p><p>Déplacer la compétence vers le domaine :</p>';
	donnees["competences"].forEach((el, i) => {
		contenuHTML += '<div class="bFormation bGris main" onclick="deplacerCompGestion('+domaine+','+competence+','+i+')">'+ el["domaine"] +'</div><br>';
	});
	contenuHTML += '<br><br><div class="bFormation main bViolet" onclick="afficherEcran(ecranCompetences)">Annuler</div></div>';
	messages.innerHTML = contenuHTML;
	messages.style.display = "block";
}

function deplacerCompGestion(domaineAvant, competence, domaineApres) {
	let donneesComp = JSON.parse(JSON.stringify(donnees["competences"][domaineAvant]["competences"][competence]));
	donnees["competences"][domaineAvant]["competences"].splice(competence, 1);
	donnees["competences"][domaineApres]["competences"].push(donneesComp);
		donnees["eleves"].forEach((eleve) => {
			eleve["Competences"].forEach((element) => {
				if (element["domaine"] === donnees["competences"][domaineAvant]["domaine"] && element["nom"] === donneesComp["nom"]) {
					element["domaine"] = element["domaine"].replaceAll(donnees["competences"][domaineAvant]["domaine"],donnees["competences"][domaineApres]["domaine"]);
				}
			});
		});
	afficherEcran(ecranCompetences);
}

/* Paramètres */

function genererParametres() {
	listeParametres.innerHTML = '';
	ecranParametresNom.value = donnees["parametres"]["ecole"];
	ecranParametresAnnee.value = donnees["parametres"]["anneeScolaire"];
	ecranParametresClasse.value = donnees["parametres"]["classe"];
	ecranParametresIntro.value = donnees["parametres"]["motIntroduction"].replaceAll('<br>', '\n');
	if (donnees["parametres"]["couleur"] == undefined) {
		couleurBaniere.value = '#EAF2F8';
		Menu.style.backgroundColor = '#EAF2F8';
	} else {
		couleurBaniere.value = donnees["parametres"]["couleur"];
		Menu.style.backgroundColor = donnees["parametres"]["couleur"];
	}
	if (donnees["parametres"]["imageIllustration"] !== undefined && donnees["parametres"]["imageIllustration"] !== '') {
		outputImageIllustration.innerHTML = '<div class="centrer"><img src="'+donnees["parametres"]["imageIllustration"]+'" /><br><div class="bFormation main bViolet" onclick="supprimerImageIllustration()">Supprimer l\'image</div></div>';
	} else {
		outputImageIllustration.innerHTML = '<p class="centrer">Aucune image d\'illustration pour le moment.</p>';
	}
}

function changerNomEcole(event) {
	donnees["parametres"]["ecole"] = event.target.value;
}

function changerAnneeSco(event) {
	donnees["parametres"]["anneeScolaire"] = event.target.value;
}

function changerNomClasse(event) {
	donnees["parametres"]["classe"] = event.target.value;
}

function changerTextIntro(event) {
	donnees["parametres"]["motIntroduction"] = event.target.value.replaceAll('\n', '<br>');
}

function changerCouleurBaniere(event) {
	donnees["parametres"]["couleur"] = event.target.value;
	Menu.style.backgroundColor = event.target.value;
}

function img2base64(event) {
    let file = event.target.files[0];
    if (file) {
        let reader = new FileReader();
        reader.onload = function(e) {
            let img = new Image();
            img.onload = function() {
                let width = img.width;
                let height = img.height;
                // Check if the image needs to be resized
                if (width > 600) {
                    height = Math.round((600 / width) * height);
                    width = 600;
				}
				if (height > 300) {
					width = Math.round((300 / height) * width);
					height = 300;
				}
                // Create a canvas to draw the resized image
                const canvas = document.createElement('canvas');
                canvas.width = width;
                canvas.height = height;
                const ctx = canvas.getContext('2d');
                ctx.drawImage(img, 0, 0, width, height);
                // Convert the resized image to Base64
                const base64String = canvas.toDataURL('image/jpeg');
                outputImageIllustration.innerHTML = `<div class="centrer"><img src="${base64String}"></div>`;
				donnees["parametres"]["imageIllustration"] = base64String;
            };
            img.src = e.target.result;
        };
        reader.readAsDataURL(file);
    }
}

function supprimerImageIllustration() {
	donnees["parametres"]["imageIllustration"] = '';
	afficherEcran(ecranParametres);
}

/* Nettoyage complet du contenu des livrets élèves */

function nettoyageLivretsEleves() {
	donnees["eleves"].forEach((el) => {
		if (el["Competences"]) {
			for (let i2 = el["Competences"].length - 1; i2 >= 0; i2--) {
				let el2 = el["Competences"][i2];
				let idDomaine = donnees["competences"].findIndex(recherche => recherche["domaine"] === el2["domaine"]);
				if (idDomaine === -1 || donnees["competences"][idDomaine]["competences"].findIndex(recherche2 => recherche2["nom"] === el2["nom"]) === -1) {
					el["Competences"].splice(i2, 1);
				}
			}
		}
		if (el["Images"]) {
			for (let i2 = el["Images"].length - 1; i2 >= 0; i2--) {
				let el2 = el["Images"][i2];
				if (donnees["competences"].findIndex(recherche => recherche["domaine"] === el2["domaine"]) === -1) {
					el["Images"].splice(i2, 1);
				}
			}
		}
		if (el["CommentairesDomaine"]) {
			for (let i2 = el["CommentairesDomaine"].length - 1; i2 >= 0; i2--) {
				let el2 = el["CommentairesDomaine"][i2];
				if (donnees["competences"].findIndex(recherche => recherche["domaine"] === el2["domaine"]) === -1) {
					el["CommentairesDomaine"].splice(i2, 1);
				}
			}
		}
	});
	messages.innerHTML = '<div class="boite bBleu"><p>Les données inutilisées des livrets élèves ont été nettoyées.</p></div>';
	messages.style.display = 'block';
}

function nettoyageLivretsElevesTotal() {
	donnees["eleves"].forEach((el, i) => {
		if (el["Competences"]) {
			el["Competences"].splice(0, el["Competences"].length);
		}
		if (el["Commentaires"]) {
			el["Commentaires"].splice(0, el["Commentaires"].length);
		}
		if (el["Images"]) {
			el["Images"].splice(0, el["Images"].length);
		}
		if (el["CommentairesDomaine"]) {
			el["CommentairesDomaine"].splice(0, el["CommentairesDomaine"].length);
		}
	});
	messages.innerHTML = '<div class="boite bBleu"><p>Les données des livrets élèves ont été nettoyées.</p></div>';
	messages.style.display = 'block';
}

/* Imports, Exports, Sauvegarde */

function sauvegarder() {
	let contenuSauvegarde = JSON.stringify(donnees);
	let blob = new Blob([contenuSauvegarde], {
		type: "application/json;charset=utf-8"
	});
	let url = URL.createObjectURL(blob);
	let a = document.createElement("a");
	a.href = url;
	a.download = "MonLivretMaternelle - "+day+"-"+month+"-"+year+".json";
	a.click();
	URL.revokeObjectURL(url);
	majArchive();
	sauvegardeLocale();
}

function sauvegardeLocale() {
	let dateSauv = new Date();
	const localeString = dateSauv.toLocaleString('fr-FR', {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit',
		hour: '2-digit',
		minute: '2-digit',
		second: '2-digit',
		hour12: false // Utiliser le format 24 heures
	});
	localforage.setItem('date', localeString);
	localforage.setItem('donnees', JSON.stringify(donnees));
}

function effacerSauvegardeLocale() {
	localforage.clear();
	initialisation();
}